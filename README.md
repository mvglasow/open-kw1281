# Open KW1281

This is a very rudimentary effort to get to a working FOSS implementation of the KW1281 protocol, and possibly a diagnostic/programming utility on top of it.

The aim is to have a cross-platform library, written in either C or Java and capable of running on all major platforms, and optionally a GUI for select platforms.

Currently I am in the information gathering stage. If you have any helpful resources to contribute, please open an issue or merge request so I can include them.

Everything I have gathered so far can be found in the wiki.

## What is KW1281?

KW1281, or KWP1281 (KWP being short for Keyword Protocol) is a vendor-specific protocol that is widely used by Volkswagen Group vehicles to communicate with various electronic control units in the vehicle. It is what repair shops use to diagnose malfunctions or change settings on said control units.

## How does KW1281 compare to OBD-II?

Both use the same plug (usually located in the driver footwell), but that is about it. They use different pins, different signaling and are fundamentally different protocols.

OBD-II is standardized across all vehicles. It supports a standardized set of commands and fault codes, and is limited to the engine control module.

KW1281 is vendor-specific. Commands, codes etc. are up to the vendor, and it can address a number of control units in the vehicle.